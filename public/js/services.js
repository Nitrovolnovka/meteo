'use strict';

var meService = angular.module('meService', ['ngResource']);

meService.factory('Resources', ['$resource', function($resource) {
    return $resource('/resources', {}, {
        get: {
            method: 'GET', 
            isArray: false,
            timeout: 600000
        },
        save: {
            method: 'POST',
            isArray: false,
            timeout: 600000
        }
    });
}]);
