'use strict';

/* App Module */

var meteoApp = angular.module('meteoApp', [
    'ngRoute',

    'meteoAppControllers',
    'meteoDirectives',
    'meService'
]);

meteoApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/', {
            templateUrl: 'partials/main-page.html',
            controller: 'MainCtrl'
        }).
        when('/resources', {
            templateUrl: 'partials/resources.html',
            controller: 'ResourcesCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
}]);
