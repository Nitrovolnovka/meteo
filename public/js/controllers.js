'use strict';

var meteoApp = angular.module('meteoAppControllers', ['meteoDirectives']);

meteoApp
//Main page Controller
.controller('MainCtrl', function ($scope, $rootScope) {
    $rootScope.page_title = "Main";
    $scope.chartData = [{
        id: "point_1",
        value: "45"
    }, {
        id: "point_2",
        value: "33"
    }];
    $rootScope.$broadcast("meSpinner::hide");
    $rootScope.$broadcast("meHeader::home_active");

    //ToDo get temp log charts data
    //ToDo init chatrs
    //ToDo get weather
    //ToDo get current sensors data
    console.log('MainCtrl');
})
//Resources Form Page Controller
.controller('ResourcesCtrl', function($scope, $rootScope, Resources) {
    var deleted,
        init = function() {
            var p = Resources.get().$promise;

            p.then(function(res) {
                $scope.data = res.dbData;
                $scope.lib = {};
                $scope.default_data = angular.copy(res.dbData);
                _.each(res.daemonData, function(drv) {
                    var role = drv.role, drv_name = drv.name;

                    delete drv.role;

                    if (typeof $scope.lib[role] === "undefined") {
                        $scope.lib[role] = {
                            name: role,
                            value: role,
                            drvs: {}
                        };
                    }
                    $scope.lib[role].drvs[drv_name] = {
                        name: drv.name,
                        addresses: {}
                    };
                    _.each(drv.addresses, function(item) {
                        $scope.lib[role].drvs[drv_name].addresses[item] = {
                            value: item
                        };
                    });
                });
                console.log(">>>drv lib", $scope.lib);
                console.log(">>>db data", $scope.data);
            }, function(err) {
                console.error(err);
            });
            p.finally(function() {
                $rootScope.$broadcast("meSpinner::hide");
            });
            console.log("Scope ResourceCtrl");
            $rootScope.$broadcast("meHeader::resource_active");

            deleted = [];
        };
    
    $rootScope.page_title = "Resources";
    
    $scope.resourceStatus = [{
        value: 0,
        name: "0 - Недоступен"
    }, {
        value: 1,
        name: "1 - Неактивен"
    }, {
        value: 2,
        name: "2 - Активен"
    }];
        
    $scope.selected_role = undefined;
    $scope.selected_drv = undefined;
    
    $scope.save = function(e) {
        var data = {
            save: $scope.data,
            deleted: deleted
        }, p;
        $rootScope.$broadcast("meSpinner::show");
        p = Resources.save(data).$promise;
                
        p.then(function(res) {
            if (res.err) {
                console.error(res.err);
            }
            console.log(res);
        }, function(err) {
            console.error(err);
        });
        p.finally(function() {
            init();
        });
    };
    
    $scope.resetForm = function(form) {
        alert("reset");
        if (form) {
            form.$setPristine();
            form.$setUntouched();
        }
        $scope.data = angular.copy($scope.default_data);
        deleted = [];
    };
    
    $scope.addNewItem = function() {
        if (typeof $scope.data[$scope.selected_role] === "undefined") {
            $scope.data[$scope.selected_role] = {};
        }
        if (typeof $scope.data[$scope.selected_role][$scope.selected_drv] === "undefined") {
            $scope.data[$scope.selected_role][$scope.selected_drv] = [];
        }
        $scope.data[$scope.selected_role][$scope.selected_drv].push({
            name: "",
            address: "",
            status: "",
            actual_val: ""
        });
    };
    
    $scope.optionDisabled = function(address, drv, role, $$hashKey) {
        var found = false;
        try {
            _.each($scope.data[role][drv], function(resource) {
                if (!found && resource.address === address && resource.$$hashKey !== $$hashKey) {
                    found = true;
                    return false;
                } 
            });
        } catch(e) {
            console.log("~~~e", e.stack);
        } finally {
            return found;
        }
    };
    
    $scope.removeItem = function(role, drv, key, id) {
        try {
            console.log(role, $scope.data[role]);
            console.log(drv, $scope.data[role][drv]);
            console.log(key, $scope.data[role][drv][key]);
            $scope.data[role][drv].splice(key, 1);
            if (_.isEmpty($scope.data[role][drv])) {
                delete $scope.data[role][drv];
            }
            if (_.isEmpty($scope.data[role])) {
                delete $scope.data[role];
            }
            if (typeof id !== "undefined" && id !== "") {
                deleted.push(id);
            }
            console.log(deleted);
        } catch (e) {
            console.dir(e);
            console.log("This data doesn't exist");
        }
    };
    
    init();
});
