'use strict';

var meDirectives = angular.module('meteoDirectives', []);

meDirectives
.constant('_', window._)
.directive('meSpinner', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/spinner.html',
        transclude: true,
        replace: true,
        link: function(scope, element, attrs) {
            scope.spinner_hide = false;

            scope.$on('meSpinner::hide', function() {
                element.css({display: "none"});
            });
            scope.$on('meSpinner::show', function() {
                element.css({display: ""});
            });
        }
    };
})
.directive('meHeader', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/header.html',
        transclude: true,
        replace: true,
        link: function(scope, element, attrs) {
            var uncheckAll = function() {
                _.each(scope.items, function(item, key) {
                    scope.items[key] = false;
                });
            };
            
            scope.items = {
                home: false,
                resources: false
            };
            
            scope.$on('meHeader::home_active', function() {
                setTimeout(function() {
                    uncheckAll();
                    scope.items.home = true;
                    console.log("here");
                    console.log("items", scope.items);
                });
            });
            scope.$on('meHeader::resources_active', function() {
                setTimeout(function() {
                    uncheckAll();
                    scope.items.resources = true;
                });
            });
        }
    };
});
