#!/usr/bin/env node
'use strict';

// require libs
var config = require('../config'),
    _console = require("../modules/log")(config.debug.api_filename),
    opt = require('node-getopt'),
    sqlite3 = require('sqlite3'),
    _ = require('lodash'),
    db, fifo, tables, api, options;

try {

    // require local resources
    db = require('../modules/db');
    fifo = require('../modules/fifo');
    tables = {
        resources: "resources"
    };
    
    // main API functions
    api = {
        /**
        @function   getResources 
        @argument {array} drv [drv-types] 
        @returns {collection} [resource list]Collection [{
            "id" - int, db id 
            "drv_type" - str, id of driver type
            "goal_type" - str, the goal of this resource
        }]         
        */
        getResources: function(options) {
//            var drv = options.drv instanceof Array ? options.drv : [options.drv];

            db.all({
//                query: 'SELECT * FROM `resources` WHERE resources.drv IN ( %%drv%% );'.replace("%%drv%%", function() {
//                    var q = [], i = 0, l = drv.length;
//                    
//                    for (; i < l; i++) {
//                        q.push('?');
//                    }
//                    
//                    return q.join(",");
//                }),
//                params: drv,
                query: 'SELECT * FROM `resources`;',
                params: [],
                callback: function(err, res) {
                    if (err) {
                        throw err;
                    }
                    _.map(res, function(item) {
                        delete item.actual_val;
                    });
                    console.log(JSON.stringify(res));
                }
            });
        },
        /**
        @function reportResource
        @argument {int} resource.id
        @argument {string} msg [OK, fail]
        @argument {int} val - value of resource
        @returns {collection} [resource list]: [{
            "id" - int, db id 
            "drv_type" - str, id of driver type
            "goal_type" - str, the goal of this resource
        }]         
         */
        reportResource: function(options) {
            var q;
            if (options.status) {
                q = {
                    query: 'UPDATE resources SET status = ? WHERE id = ?;',
                    params: [options.status, options.id]
                };
            } else if (options.actual) {
                q = {
                    query: 'UPDATE resources SET actual_val = ? WHERE id = ?;',
                    params: [options.actual, options.id]
                };
            }
            q.callback = function(err, msg) {
                if (err) {
                    _console.error('error', err);
                    return;
                }
                fifo('config');
            };
            db.cud(q);
        },
        /**
        @function reportManualTChange
        @argument {Object} [options] options.val 
         */
        reportManualTChange: function(options) {            
            db.cud({
                query: "INSERT INTO `schedule` (timestamp, value) VALUES (CURRENT_TIMESTAMP, ?)",
                params: [options.val],
                callback: function(err, res) {
                    if (err) throw err;
                    fifo('schedule');
                }
            });
        },
        /**
        @function getWeather
        @returns {Collection} [{
          "datetime": timestamp,
           "temperature": int (*10)
           "index": int
        }] - the whole list
         */
        getWeather: function() {
            db.all({
                query: 'SELECT * FROM `weather`;',
                callback: function(err, res) {
                    if (err) {
                        throw err;
                    }
                    console.log(JSON.stringify(res));
                }
            });
        },
        /**
         * 
         * @param {type} cli_args
         * @returns {unresolved}
         */
        reportLog: function(options) {
            db.cud({
                query: "INSERT INTO `logs` (resource_id, timestamp, value) VALUES (?, CURRENT_TIMESTAMP, ?)",
                params: [options.resource_id, options.value],
                callback: function(err) {
                    if (err) throw err;
                }
            });
        },
        /**
         * Parsing arguments from cli.
         */
        parseOptions: function(cli_args) {
            var splited, waiting_next = false, tmp = {}, splited_val;

            _.each(cli_args, function(arg) {
                if(arg.indexOf("=") >= 0) {
                    splited = arg.split("=");
                    splited_val = splited[1].split(",");
                    if (splited_val.length < 2) {
                        tmp[splited[0].replace(/^[-=\s]*/mg, "")] = splited[1];
                    } else {
                        tmp[splited[0].replace(/^[-=\s]*/mg, "")] = splited_val;
                    }
                    waiting_next = false;
                } else {
                    if (waiting_next !== false) {
                        if (arg.length === arg.replace(/^[-=\s]*/mg, "").length) {
                            //arg is val
                            splited_val = arg.split(",");
                            if (splited_val.length < 2) {
                                tmp[waiting_next] = arg;
                            } else {
                                tmp[waiting_next] = splited_val;
                            }
                            waiting_next = false;
                        } else {
                            //arg is new key
                            tmp[waiting_next] = true;
                            waiting_next = arg.replace(/^[-=\s]*/mg, "");
                        }
                    } else {
                        waiting_next = arg.replace(/^[-=\s]*/mg, "");
                    }
                }
            });

            if (waiting_next && typeof tmp[waiting_next] === "undefined") {
                tmp[waiting_next] = true;
            } 
            return tmp;
        }

    }
    ;

    options = api.parseOptions(process.argv.slice(2));
    _console.log("~~~options", options);
    api[options.method](options);
} catch (e) {
    _console.log("error: ", process.argv.join(" "), e.stack);
//    console.error("Error in executing" + process.argv.join(" ") + ". " + e.stack);
}
