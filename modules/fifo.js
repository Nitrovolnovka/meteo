'use strict';

var fs = require('fs'),
    fifo_path = require('../config').fifo_path;

module.exports = function(msg) {
    fs.open(fifo_path, 'w', function (err, fd) {
        if (err) throw err;
        fs.write(fd, msg, function(err) {
            if (err) throw err;
            fs.close(fd);
        });
    });
};
