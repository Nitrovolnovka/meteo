'use strict';

var 
fs = require('fs'),
path = require('path'),
exec = require('child_process').execSync,
uuid = require('uuid'),
//os = require('os'),
_ = require('lodash'),

config = require('../config'),
Console = console.Console,

checkLogFile = function(file) {
    var stats = fs.statSync(file), 
        fileSize = stats["size"],
        tmpid;

    if (fileSize > config.debug.max_file_size) {
        tmpid = uuid.v1();
        exec('mv ' + file + ' ' + file.split('.log').join("_" + tmpid + ".log"));
    }
};

module.exports = function(name) {
    try {
        var filename = path.resolve(__dirname, "..", "logs", name),
            output = fs.createWriteStream(filename, {flags:'a'}),
            _console = new Console(output, output), 
            oldLog = _console.log,
            newLog = function() {
                var args = (arguments.length === 1?[arguments[0]]:Array.apply(null, arguments));

                args.unshift(new Date().toUTCString());
                oldLog.apply(_console, args);
            };

        _console.log = newLog;
        checkLogFile(filename);

        return _console;
    } catch (e) {
        console.error(e.stack);
    }
//
//    return function() {
//        var args = arguments, res = [];
//        _.each(args, function(item){
//            if (item instanceof Object || item instanceof Array) {
//                res.push(JSON.stringify(item));
//            } else {
//                res.push(item);
//            }
//        });
//        fs.open(filename, 'a', function (err, fd) {
//            if (err) throw err;
//            fs.write(fd, new Date().toUTCString() + ' ~~ ' + res.join(" ") + os.EOL, function(err) {
//                if (err) throw err;
//                fs.close(fd, function(err) {
//                    checkLogFile();
//                });
//            });
//        });
//    };
};
