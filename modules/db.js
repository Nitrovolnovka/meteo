'use strict';

var
sqlite = require('sqlite3').verbose(),
path = require("path"),
config = require(path.resolve(__dirname, "../config.json")),
db;

try {
    db = new sqlite.Database(path.resolve(__dirname, "..", config.db.db_name));
} catch(e) {
    throw e;
}
module.exports = {
    //select all function
    all: function(query_obj) {
        db.serialize(function() {
            db.all(query_obj.query, query_obj.params, query_obj.callback);
        });
    },
    //select each function
    each: function(query_obj) {
        db.serialize(function() {
            db.each(query_obj.query, query_obj.params, query_obj.callback);
        });
    },
    
    
    //insert/update/delete function
    cud: function(query_obj) {
        db.serialize(function() {
            db.run(query_obj.query, query_obj.params, query_obj.callback);
        });
    },
    //close connection to db
    close: function() {
        db.close();
    }
};
