'use strict';

var db = require('./db'),
    _ = require('lodash'),
    spawn = require('child_process').spawn,
    config = require("../config"),
    
    _console = require("../modules/log")(config.debug.service_filename);

module.exports = {
    index: function(req, res, next) {
        res.sendFile('./public/index.html');
    },
    getResources: function(req,res,next) {
        try {
            db.all({
                query: "SELECT * FROM `resources`;",
                params: [],
                callback: function(err, data) {
                    var object = {
                            dbData: {},
                            daemonData: {}
                        }, lsres, lsresData = [];

                    if (err) throw err;
                    _console.log("getResources", data);
                    _.each(data, function(item) {
                        var role = item.role,
                            drv = item.drv;

                        _console.log("item", item);

                        delete item.role;
                        delete item.drv;

                        if (typeof (object.dbData[role]) === "undefined") {
                            object.dbData[role] = {};
                        }
                        if (typeof (object.dbData[role][drv]) === "undefined") {
                            object.dbData[role][drv] = [];
                        }
                        object.dbData[role][drv].push(item);
                    });
                    _console.log("getResources object", JSON.stringify(object));
                    lsres = spawn(config.daemon_path, []);

                    lsres.stdout.on('data', function(data) {
                        lsresData.push(data);
//                        _console.log('stdout::', data);
                    });

                    lsres.stderr.on('data', function(data) {
                        _console.log('stderr::', data);
                    });

                    lsres.on('close', function(code) {
                        var data = JSON.parse(lsresData.join('').toString());
                        _console.log("data lsres ", JSON.stringify(data));
                        object.daemonData = data;
//                        object.daemonData = require("../tmp_daemon");
                        res.json(object);
                        res.end;
                        _console.log('child process exited with code', code);
                    });
                }
            });
        } catch(e) {
            res.json({err: e});
            res.end();
        }
    },
    postResources: function(req,res,next) {
        var to_save = req.body.save,
            to_delete = req.body.deleted;
        try {
            _console.log("data:", JSON.stringify(req.body));

            _.each(to_delete, function(id) {
                db.cud({
                    query: "DELETE FROM `resources` WHERE id = ?;",
                    params: [id],
                    callback: function(err, res) {
                        if (err) throw err;
                        _console.log("deleted", id, res);
                    }
                });
            });

            _.each(to_save, function(drivers, role) {
                _.each(drivers, function(resources, driver) {
                    _.each(resources, function (resource) {
                        if (typeof resource.id === "undefined") {
                            db.cud({
                                query: "INSERT INTO `resources` (`drv`, `role`, `status`, `name`, `address`) VALUES (?, ?, ?, ?, ?);",
                                params: [driver, role, resource.status, resource.name, resource.address],
                                callback: function(err, res) {
                                    if (err) throw err;
                                    _console.log("inserted", JSON.stringify(resource), driver, role, res);
                                }
                            });
                        } else {
                            db.cud({
                                query: "UPDATE `resources` SET `drv` = ?, `role` = ?, `status` = ?, `name` = ?, `address` = ? WHERE id = ?;",
                                params: [driver, role, resource.status, resource.name, resource.address, resource.id],
                                callback: function(err, res) {
                                    if (err) throw err;
                                    _console.log("updated", JSON.stringify(resource), driver, role, res);
                                }
                            });
                        }
                    });
                });
            });

            res.json({status:"received"});
            res.end();
        } catch (e) {
            res.json({err: e.stack});
            res.end();
        }
    }
};