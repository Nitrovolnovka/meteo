CREATE TABLE `resources` (
    `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
    `drv`	TEXT,
    `role`	TEXT,
    `status`	INTEGER,
    `name`	TEXT,
    `address`	TEXT,
    `actual_val`	INTEGER
);

CREATE TABLE `weather` (
    `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
    `datetime`	NUMERIC,
    `temperature`	INTEGER,
    `index`	INTEGER
);

CREATE TABLE `schedule` (
    `day`	INTEGER,
    `timestamp`	INTEGER,
    `value`	INTEGER
);

CREATE TABLE `logs` (
    `id`	INTEGER,
    `resource_id`	INTEGER,
    `value`	INTEGER,
    `timestamp`	INTEGER,
    PRIMARY KEY(id)
);